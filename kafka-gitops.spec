%define debug_package %{nil}

Name:           kafka-gitops
Version:        0.2.15
Release:        0%{?dist}
Summary:        Manage Apache Kafka topics and generate ACLs through a desired state file

License:        ASL 2.0
URL:            https://devshawn.github.io/kafka-gitops
Source0:        https://github.com/devshawn/%{name}/releases/download/%{version}/kafka-gitops.zip

%description
Manage Apache Kafka topics and generate 
ACLs through a desired state file.

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_bindir}
install -m 755 %{name} %{buildroot}/%{_bindir}/%{name}

%files
%{_bindir}/%{name}

%changelog
* Thu Sep 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM